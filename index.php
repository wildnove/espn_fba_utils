<?php
require('config/config.inc.php');
require('include/SqliteHandler.php');
require('include/ScoreboardRow.php');

$season = SEASON;
echo '
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Espn fba utils - virtual matches results</title>
        <link href="lib/bootstrap-3.3.6/css/bootstrap.min.css" rel="stylesheet">

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->';
if (GOOGLE_ANALYTICS_TRACKING_ID)
    echo '
        <script type="text/javascript">
            (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'__gaTracker\');

            __gaTracker(\'create\', \'' . GOOGLE_ANALYTICS_TRACKING_ID . '\', \'auto\');
            __gaTracker(\'set\', \'forceSSL\', true);
            __gaTracker(\'send\',\'pageview\');

        </script>';
echo '
    </head>
    <body style="padding:10px;">';

$SqliteHandler = new SqliteHandler();
$SqliteHandler->connect();
if ($_REQUEST['operation'] == 'initDatabase') {
    $SqliteHandler->dropTables();
    $SqliteHandler->createTables();
    $SqliteHandler->populateTables();
    echo 'Ok';
} else {
    $teams = $SqliteHandler->getTeams($season);
    $scoreboards = $SqliteHandler->getScoreboards($season);

    echo '
        <form id="teamsform" action="" method="GET">';
    echo '
            <label>Team 1
                <select  class="form-control" onchange="document.getElementById(\'teamsform\').submit();return false;" name="team1">
                    <option value=""></option>';
    foreach ($teams as $team) {
        $selected = $_REQUEST['team1'] == $team['name'] ? 'selected="selected"' : '';
        echo '
                    <option ' . $selected . ' value="' . $team['name'] . '">' . $team['name'] . '</option>';
    }
    echo '
                </select>
            </label>';
    echo '
            <label>Team 2
                <select  class="form-control" onchange="document.getElementById(\'teamsform\').submit();return false;" name="team2">
                    <option value=""></option>';
    foreach ($teams as $team) {
        $selected = $_REQUEST['team2'] == $team['name'] ? 'selected="selected"' : '';
        echo '
                    <option ' . $selected . ' value="' . $team['name'] . '">' . $team['name'] . '</option>';
    }
    echo '
                </select>
            </label>';
    echo '
            <label>Scoreboard
                <select  class="form-control" onchange="document.getElementById(\'teamsform\').submit();return false;" name="scoreboard">
                    <option value=""></option>';
    foreach ($scoreboards as $scoreboard) {
        $selected = $_REQUEST['scoreboard'] == $scoreboard['scoreboard'] ? 'selected="selected"' : '';
        echo '
                    <option ' . $selected . ' value="' . $scoreboard['scoreboard'] . '">' . $scoreboard['block'] . ' match ' . $scoreboard['period'] . '</option>';
    }
    echo '
                </select>
            </label>';
    echo '
        </form>';

    if (
        ($_REQUEST['team1'] && $_REQUEST['team2'] && $_REQUEST['team1'] != $_REQUEST['team2']) ||
        ($_REQUEST['team1'] && $_REQUEST['scoreboard'] && $_REQUEST['team1'] != $_REQUEST['team2'])
    ) {
        $all_matches = $SqliteHandler->getTeamVsTeamMatches($season, $_REQUEST['team1'], $_REQUEST['team2'], $_REQUEST['scoreboard']);
        foreach ($all_matches as $match) {
            echo '
            <table class="table table-striped table-condensed">
            <caption>' . $match['season'][0] . ' - ' . $match['block'][0] . ($match['period'][0] ? ' - ' . $match['period'][0] : '') . '</caption>';
            echo '
                <tr>
                    <td width="20%">&nbsp;</td><td width="40%"><strong>' . $match['team'][0] . '</strong></td><td width="40%"><strong>' . $match['team'][1] . '</strong></td>
                </tr>';

            if (intval($match['computed_score'][0]) > intval($match['computed_score'][1])) {
                $score1 = 'label-success';
                $score2 = 'label-danger';
            } else if (intval($match['computed_score'][0]) < intval($match['computed_score'][1])) {
                $score1 = 'label-danger';
                $score2 = 'label-success';
            } else {
                $score1 = 'label-default';
                $score2 = 'label-default';
            }


            $win1 = $row[2] == 1 ? 'class="success"' : '';
            $win2 = $row[2] == 2 ? 'class="success"' : '';
            echo '
                <tr>
                    <td><strong>' . intval($match['computed_score'][0]) . '-' . intval($match['computed_score'][1]) . '-' . intval($match['computed_score'][2]) . '</strong></td><td><span class="label ' . $score1 . '">' . intval($match['computed_score'][0]) . '</span></td><td><span class="label ' . $score2 . '">' . intval($match['computed_score'][1]) . '</span></td>
                </tr>';
            unset($match['id']);
            unset($match['season']);
            unset($match['vs_team']);
            unset($match['team_score']);
            unset($match['block']);
            unset($match['period']);
            unset($match['team_wlt']);
            unset($match['team']);
            unset($match['computed_score']);

            $ScoreboardRow = new ScoreboardRow();
            foreach ($match as $field => $row) {
                $field = $ScoreboardRow->getFieldByName($field);
                $name = $field['espn'] ? $field['espn'] : $field['name'];
                $win1 = $row[2] == 1 ? 'class="success"' : '';
                $win2 = $row[2] == 2 ? 'class="success"' : '';
                echo '
                <tr>
                    <td><strong>' . $name . '</strong></td><td ' . $win1 . '>' . $row[0] . '</td><td ' . $win2 . '>' . $row[1] . '</td>
                </tr>';
            }
            echo '
            </table>

        <br/>';

        }

    }


}
$SqliteHandler->disconnect();


echo '
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="lib/bootstrap-3.3.6/js/bootstrap.min.js"></script>
    </body>
</html>';
