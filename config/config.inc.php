<?php
date_default_timezone_set('UTC');
define('SQLITE3_DB_FILENAME', 'db.sqlite');
define('GOOGLE_ANALYTICS_TRACKING_ID', 'UA-24969580-1');
define('SEASON', '2024-2025');
