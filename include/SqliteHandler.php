<?php
function __autoload($class_name)
{
    include $class_name . '.php';
}

class SqliteHandler
{
    public $fileDb = null;
    public $dbFilename = 'db.sqlite';
    public $handledObjects = array('ScoreboardRow');

    public $error = '';

    function getError()
    {
        return $this->error;
    }

    function connect()
    {
        $this->fileDb = new PDO('sqlite:' . __DIR__ . '/../db/' . $this->dbFilename);
        $this->fileDb->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    function disconnect()
    {
        $this->fileDb = null;
    }

    function reflectObject($object)
    {
        if ($object == 'ScoreboardRow')
            $object = new ScoreboardRow();
        else
            $object = new stdClass();
        return $object;
    }

    function dropTables()
    {
        foreach ($this->handledObjects as $object_name) {
            $object = $this->reflectObject($object_name);
            $stmt = "DROP TABLE IF EXISTS " . $object->table;
            $this->fileDb->exec($stmt);
        }
    }

    function createTables()
    {
        foreach ($this->handledObjects as $object_name) {
            $object = $this->reflectObject($object_name);
            $table = $object->table;
            foreach ($object->fields as $field)
                $fields .= $field['name'] . ' ' . $field['type'] . ($field['index'] == 'PRIMARY KEY' || $field['index'] == 'UNIQUE' ? ' ' . $field['index'] : '') . ($field['index'] == 'PRIMARY KEY' && $field['autoincrement'] == 'AUTOINCREMENT' ? ' ' . $field['autoincrement'] : '') . ',';
            $fields = rtrim($fields, ',');
            $stmt = "CREATE TABLE " . $table . " (" . $fields . ")";
            $this->fileDb->exec($stmt);
            foreach ($object->fields as $field) {
                if ($field['index'] == 'INDEX') {
                    $stmt = "CREATE INDEX " . $field['name'] . "_idx ON " . $table . " (" . $field['name'] . ")";
                    $this->fileDb->exec($stmt);
                }
            }
        }
    }

    function populateTables()
    {
        foreach ($this->handledObjects as $object_name) {
            $object = $this->reflectObject($object_name);
            $data = $object->parseFiles();
            foreach ($data as $results) {
                foreach ($results as $row) {
                    $this->insert($object, $row);
                }
            }
        }
    }

    function insert($object, $row)
    {
        $table = $object->table;
        $fields = implode(', ', array_keys($row));
        $binds = ':' . implode(', :', array_keys($row));
        $stmt = $this->fileDb->prepare("INSERT INTO " . $table . " (" . $fields . ") VALUES (" . $binds . ")");
        foreach ($row as $field => $value)
            $stmt->bindValue(':' . $field, $value);
        $stmt->execute();
    }

    function getTeams($season, $exclude = '')
    {
        $stmt = $this->fileDb->prepare("
            SELECT team AS name
            FROM ScoreboardRow
            WHERE season = '" . $season . "'
            AND block = 'regular-season'
            " . ($exclude ? " AND team!='" . $exclude . "' " : "") . "
            GROUP BY team ORDER BY team");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    function getScoreboards($season)
    {
        $stmt = $this->fileDb->prepare("
            SELECT block || '_' || period AS scoreboard,
            block, period
            FROM ScoreboardRow
            WHERE season = '" . $season . "'
            GROUP BY block, period
            ORDER BY scoreboard DESC");
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    function getTeamVsTeamMatches($season, $team1, $team2, $scoreboard)
    {
        $ScoreboardRow = new ScoreboardRow();
        $matches = array();
        $match = array();

        if ($scoreboard) {
            list($block, $period) = explode("_", $scoreboard);
            $scoreboards = array(0 => array('scoreboard' => $scoreboard, 'block' => $block, 'period' => $period));
        } else
            $scoreboards = $this->getScoreboards($season);

        if ($team2)
            $teams2 = array(0 => array('name' => $team2));
        else
            $teams2 = $this->getTeams($season, $exclude = $team1);

        foreach ($scoreboards as $scoreboard) {
            foreach ($teams2 as $team2) {
                $stmt = $this->fileDb->prepare("
                        SELECT *
                        FROM ScoreboardRow
                        WHERE season = '" . $season . "'
                        AND (team = '" . $team1 . "' OR team = '" . $team2['name'] . "')
                        AND block LIKE '" . $scoreboard['block'] . "' AND period LIKE '" . $scoreboard['period'] . "'");
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $ordered_rows[0] = array();
                $ordered_rows[1] = array();
                foreach ($rows as $row) {
                    if ($row['team'] == $team1)
                        $ordered_rows[0] = $row;
                    else
                        $ordered_rows[1] = $row;
                }
                foreach ($ordered_rows as $row) {
                    $column = $counter % 2;
                    $counter++;

                    foreach ($ScoreboardRow->fields as $field) {
                        $match[$field['name']][$column] = $row[$field['name']];
                        if ($column == 1 && $field['inscore']) {
                            $win1 = 1;
                            $win2 = 2;
                            if ($field['inscore'] == 'invert') {
                                $win1 = 2;
                                $win2 = 1;
                            }
                            if ($match[$field['name']][0] > $match[$field['name']][1]) {
                                $match[$field['name']][2] = $win1;
                                $match['computed_score'][$win1 - 1]++;
                            } else if ($match[$field['name']][0] < $match[$field['name']][1]) {
                                $match[$field['name']][2] = $win2;
                                $match['computed_score'][$win2 - 1]++;
                            } else {
                                $match[$field['name']][2] = 0;
                                $match['computed_score'][2]++;
                            }
                        }
                    }

                    if ($column == 1) {
                        array_push($matches, $match);
                        $match = array();
                    }
                }
            }

        }


        return $matches;
    }

}
