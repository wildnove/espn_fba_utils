<?php

class ScoreboardRow
{
    public $table = "ScoreboardRow";
    public $fields = array(
        0 => array("name" => "id", "type" => "INTEGER", "index" => "PRIMARY KEY", "autoincrement" => "AUTOINCREMENT"),
        1 => array("name" => "season", "type" => "TEXT", "index" => "INDEX"),
        2 => array("name" => "block", "type" => "TEXT", "index" => "INDEX"),
        3 => array("name" => "period", "type" => "TEXT", "index" => "INDEX"),
        4 => array("name" => "team", "espn" => "NAME", "type" => "TEXT", "index" => "INDEX"),
        5 => array("name" => "vs_team", "type" => "TEXT", "index" => "INDEX"),
        6 => array("name" => "team_wlt", "type" => "TEXT"),
        7 => array("name" => "FGP", "espn" => "FG%", "type" => "DECIMAL", "inscore" => "normal"),
        8 => array("name" => "FTP", "espn" => "FT%", "type" => "DECIMAL", "inscore" => "normal"),
        9 => array("name" => "TPM", "espn" => "3PM", "type" => "INTEGER", "inscore" => "normal"),
        10 => array("name" => "TPP", "espn" => "TP%", "type" => "DECIMAL", "inscore" => "normal"),
        11 => array("name" => "REB", "type" => "INTEGER", "inscore" => "normal"),
        12 => array("name" => "AST", "type" => "INTEGER", "inscore" => "normal"),
        13 => array("name" => "STL", "type" => "INTEGER", "inscore" => "normal"),
        14 => array("name" => "STR", "type" => "DECIMAL", "inscore" => "normal"),
        15 => array("name" => "BLK", "type" => "INTEGER", "inscore" => "normal"),
        16 => array("name" => "TOV", "espn" => "TO", "type" => "INTEGER", "inscore" => "invert"),
        17 => array("name" => "DDB", "espn" => "DD", "type" => "INTEGER", "inscore" => "normal"),
        18 => array("name" => "TDB", "espn" => "TD", "type" => "INTEGER", "inscore" => "normal"),
        19 => array("name" => "PTS", "type" => "INTEGER", "inscore" => "normal"),
        20 => array("name" => "PPM", "type" => "DECIMAL", "inscore" => "normal"),
        21 => array("name" => "team_score", "espn" => "SCORE", "type" => "TEXT"));

    function getFieldByName($name)
    {
        foreach ($this->fields as $field) {
            if ($field['name'] == $name)
                return $field;
        }
        return false;
    }

    function parseFile($tsv_file_path)
    {
        $path_info = pathinfo($tsv_file_path);
        $entry_exp = explode("_", $path_info['filename']);

        $rows = array();
        if ($entry_exp[0] == 'scoreboard') {

            $matchRowCount = 0;
            $row1 = array();
            $row2 = array();

            $handle = fopen($tsv_file_path, "r");
            while (($line = fgets($handle)) !== false) {
                $matchRowCount++;

                $line = str_replace("\n", "", $line);
                $line = str_replace("'", "`", $line);
                if ($matchRowCount == 1) {
                    continue;
                }
                if ($matchRowCount == 2) {
                    $row1['team'] = $line;
                    $row2['vs_team'] = $line;
                } else if ($matchRowCount == 3) {
                    $line = str_replace("(", "", $line);
                    $line = str_replace(")", "", $line);
                    $row1['team_wlt'] = $line;
                } else if ($matchRowCount == 4)
                    $row1['team_score'] = $line;
                else if ($matchRowCount == 5) {
                    continue;
                } else if ($matchRowCount == 6) {
                    $row2['team'] = $line;
                    $row1['vs_team'] = $line;
                } else if ($matchRowCount == 7) {
                    $line = str_replace("(", "", $line);
                    $line = str_replace(")", "", $line);
                    $row2['team_wlt'] = $line;
                } else if ($matchRowCount == 8)
                    $row2['team_score'] = $line;
                else if ($matchRowCount >= 24 && $matchRowCount <= 37) {
                    $field = $this->fields[$matchRowCount - 17];
                    if ($field['type'] == 'INTEGER')
                        $row1[$field['name']] = intval($line);
                    else if ($field['type'] == 'DECIMAL')
                        $row1[$field['name']] = floatval($line);
                } else if ($matchRowCount >= 39 && $matchRowCount <= 52) {
                    $field = $this->fields[$matchRowCount - 32];
                    if ($field['type'] == 'INTEGER')
                        $row2[$field['name']] = intval($line);
                    else if ($field['type'] == 'DECIMAL')
                        $row2[$field['name']] = floatval($line);
                } else if ($matchRowCount == 53) {
                    $row1['season'] = $entry_exp[1];
                    $row1['block'] = $entry_exp[2];
                    $row1['period'] = $entry_exp[3] != '' ? $entry_exp[3] : '';
                    $row2['season'] = $entry_exp[1];
                    $row2['block'] = $entry_exp[2];
                    $row2['period'] = $entry_exp[3] != '' ? $entry_exp[3] : '';

                    $rows[] = $row1;
                    $rows[] = $row2;
                    $matchRowCount = 0;
                    $row1 = array();
                    $row2 = array();
                }



            }
            fclose($handle);
        }

        return $rows;
    }

    function parseFile_old($tsv_file_path)
    {
        $path_info = pathinfo($tsv_file_path);
        $entry_exp = explode("_", $path_info['filename']);
        $rows = array();
        $next = 0;
        $keys = "";
        if ($entry_exp[0] == 'scoreboard') {
            $handle = fopen($tsv_file_path, "r");
            while (($line = fgets($handle)) !== false) {
                $line = str_replace("\n", "", str_replace("\t\t", "\t", $line));
                $line = explode("\t", $line);
                if ($line[0] == 'NAME') {
                    $keys = $line;
                    $next = 2;
                } else if ($next > 0) {
                    $next--;
                    $values = $line;
                    $row = array_combine($keys, $values);

                    foreach ($keys as $key) {
                        foreach ($this->fields as $field) {
                            if ($field['espn'] == $key || $field['name'] == $key) {
                                if ($field['espn'] == $key) {
                                    $row[$field['name']] = $row[$field['espn']];
                                    unset($row[$field['espn']]);
                                    $key = $field['name'];
                                }
                                if ($field['type'] == 'INTEGER')
                                    $row[$key] = intval($row[$key]);
                                else if ($field['type'] == 'DECIMAL')
                                    $row[$key] = floatval($row[$key]);
                            }
                        }
                    }

                    $exp = explode(" (", $row['team']);
                    $row['team'] = $exp[0];
                    $row['team_wlt'] = str_replace(")", "", $exp[1]);

                    $row['season'] = $entry_exp[1];
                    $row['block'] = $entry_exp[2];
                    $row['period'] = $entry_exp[3] != '' ? $entry_exp[3] : '';

                    if ($next == 0) {
                        $rows[count($rows) - 1]['vs_team'] = $row['team'];
                        $row['vs_team'] = $rows[count($rows) - 1]['team'];
                    }

                    array_push($rows, $row);
                }
            }
            fclose($handle);
        }
        return $rows;
    }

    function parseFiles()
    {
        $tvs_dir = getcwd() . '/tsv/';
        $data = array();
        if ($handle = opendir($tvs_dir)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $rows = $this->parseFile($tvs_dir . $entry);
                    array_push($data, $rows);
                }
            }
            closedir($handle);
        }
        return $data;
    }
}
